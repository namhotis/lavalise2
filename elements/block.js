import MainGame from "../main"
import { modifyArrayValue } from "../utils"

class Block {
    constructor(x, y) {
        this.game = new MainGame()

        this.x = x
        this.y = y

        this.isDown = false

        this.init()
    }

    init() {
        this.setInstance()
        this.setEventListeners()
    }

    setInstance() {
        this.instance = this.game.physics.add.sprite(this.game.TILE_SCALE * this.game.scaleTileX * (this.x), this.game.TILE_SCALE * this.game.scaleTileY * (this.y), 'block').setInteractive({ draggable: true, cursor: 'pointer' })
        this.instance.setOrigin(0, 0)
        this.instance.setScale(this.game.scaleTileX, this.game.scaleTileY)
    }

    setEventListeners() {
        this.instance.on('dragend', (pointer) => {
            this.dragValue = {
                x: pointer.upX - pointer.downX,
                y: pointer.upY - pointer.downY, 
            }

            this.checkDirection()
        }, this.game);
        
        console.log(this.instance)
        this.instance.on('gameobjectmove', (pointer) => {
            console.log("??????,")
        }, this.game);
    }

    checkDirection() {
        this.moveDirection = null

        if (Math.abs(this.dragValue.x) > Math.abs(this.dragValue.y)) {
            if (this.dragValue.x > 30) {
                this.moveDirection = "right"
            } else if (this.dragValue.x < -30) {
                this.moveDirection = "left"
            }
        } else {
            if (this.dragValue.y > 30) {
                this.moveDirection = "bottom"
            } else if (this.dragValue.y < - 30) {
                this.moveDirection = "top"
            }
        }

        if (this.moveDirection !== null) {
            this.checkIfCanMove()
        }
    }

    checkIfCanMove() {
        if (["top", "bottom", "left", "right"].includes(this.moveDirection)) {
            const dx = { top: 0, bottom: 0, left: -1, right: 1 }[this.moveDirection];
            const dy = { top: -1, bottom: 1, left: 0, right: 0 }[this.moveDirection];
        
            const newX = this.x + dx;
            const newY = this.y + dy;

            if (this.game.plateau.instance[newY][newX] === " ") {
                console.log(newX, newY)
                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, newY, newX, "C");
                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ");
                this.x = newX;
                this.y = newY;
        
                this.tween = this.game.tweens.add({
                    targets: this.instance,
                    x: this.game.TILE_SCALE * this.game.scaleTileX * this.x,
                    y: this.game.TILE_SCALE * this.game.scaleTileY * this.y,
                    duration: 1000,
                    ease: 'Cubic'
                });
            }
        }

        console.log(this.game.plateau.instance)
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Block
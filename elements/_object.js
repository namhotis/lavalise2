import MainGame from "../main"
import {
  modifyArrayValue,
  showPlateauInConsole,
  generateRandomString,
} from "../utils";

class ObjectToPlace {
  constructor(x, y, schema, assetName) {
    this.game = new MainGame();

    this.x = x;
    this.y = y;
    this.schema = schema
    this.assetName = assetName

    this.isDown = false;

    this.init();
  }

  init() {
    this.setInstance();
    this.setShadow();
    this.setEventListeners();
  }

  setInstance() {
    this.RATIO_GLOBAL_X = this.game.TILE_SCALE * this.game.scaleTileX;
    this.RATIO_GLOBAL_Y = this.game.TILE_SCALE * this.game.scaleTileY;

    this.instance = this.game.physics.add
      .sprite(
        this.RATIO_GLOBAL_X * this.x,
        this.RATIO_GLOBAL_Y * this.y + 200,
        this.assetName
      )
      .setInteractive({ draggable: true, cursor: "pointer" });
    this.instance.setOrigin(0, 0);
    this.instance.setDepth(1);
    this.instance.setScale(this.game.scaleTileX, this.game.scaleTileY);

    this.instance.letter = generateRandomString();

    while (this.game.letters.includes(this.instance.letter)) {
        this.instance.letter = generateRandomString();
    }

    this.instance.schema = this.schema.map((_row, idx) => {
        return _row.map((_col, _idx) => {
            console.log(_col)
            if (_col === "o") {
                return this.instance.letter
            } else {
                return " "
            }
        })
    })

    this.instance.startPiece = {
      x: this.x,
      y: this.y,
    };
  }

  setShadow() {
    this.instance.shadow = this.game.physics.add.sprite(
      this.RATIO_GLOBAL_X * this.instance.startPiece.x,
      this.RATIO_GLOBAL_Y * this.instance.startPiece.y + 200,
      this.assetName
    );
    this.instance.shadow.setOrigin(0, 0);
    this.instance.shadow.alpha = 0.4;
    this.instance.shadow.setScale(this.game.scaleTileX, this.game.scaleTileY);
  }

  onDragStart(pointer, dragX, dragY) {
    this.instance.setDepth(3);

    this.instance.startPiece = {
      x: this.instance.pieceSelected?.x || (this.x),
      y: this.instance.pieceSelected?.y || (this.y + 2),
    };

    this.instance.differencePiece = {
      x: dragX,
      y: dragY,
    };

    
    this.eraseOldBlockInPlateauInstance();
  }

  onDrag(pointer) {
    this.instance.piecePosition = {
      x: pointer.x - this.instance.differencePiece.x,
      y: pointer.y - this.instance.differencePiece.y,
    };

    this.instance.pieceSelected = {
      x: Math.round(this.instance.piecePosition.x / this.RATIO_GLOBAL_X),
      y: Math.round(
        this.instance.piecePosition.y / (this.game.TILE_SCALE * this.game.scaleTileY)
      ),
    };

    this.instance.shadow.x = this.instance.pieceSelected.x * this.RATIO_GLOBAL_X;
    this.instance.shadow.y = this.instance.pieceSelected.y * this.RATIO_GLOBAL_Y;

    this.instance.setPosition(this.instance.piecePosition.x, this.instance.piecePosition.y);
  }

  setEventListeners() {
    this.instance.on("dragstart", (pointer, dragX, dragY) => {
        console.log(this.instance)
        this.onDragStart(pointer, dragX, dragY);
    }, this.game);

    this.instance.on(
      "drag",
      (pointer) => {
        this.onDrag(pointer)
      },
      this.game
    );

    this.instance.on(
      "dragend",
      (pointer) => {
        this.instance.setDepth(1);
        this.positionateObject();
      },
      this.game
    );
  }

  resetShadowPositionAfterCantPlace() {
    this.instance.shadow.x = this.RATIO_GLOBAL_X * this.instance.ifErrorX.x;
    this.instance.shadow.y = this.RATIO_GLOBAL_Y * this.instance.ifErrorX.y;
  }

  setNewBlockInPlateauInstance() {
    this.instance.schema.forEach((_row, idx) => {
        console.log(_row)
      _row.forEach((_col, _idx) => {
        if (_col !== " ") {
            console.log(_col)
            this.game.plateau.instance = modifyArrayValue(
              this.game.plateau.instance,
              this.instance.pieceSelected.y + idx - 2,
              this.instance.pieceSelected.x + _idx,
              this.instance.letter
            );
        }
      });
    });

    console.log(showPlateauInConsole(this.game.plateau.instance))
  }

  eraseOldBlockInPlateauInstance() {
    this.instance.schema.forEach((_row, idx) => {
      _row.forEach((_col, _idx) => {
        if (this.game.plateau.instance[this.instance.startPiece.y + idx - 2]) {
          if (
            this.game.plateau.instance[this.instance.startPiece.y + idx - 2][
              this.instance.startPiece.x + _idx
            ]
          ) {
            if (
              this.game.plateau.instance[this.instance.startPiece.y + idx - 2][
                this.instance.startPiece.x + _idx
              ][0]
            ) {
              if (_col !== " ") {
                this.game.plateau.instance = modifyArrayValue(
                  this.game.plateau.instance,
                  this.instance.startPiece.y + idx - 2,
                  this.instance.startPiece.x + _idx,
                  "x"
                );
              }
            }
          }
        }
      });
    });
  }

  resetPositionAfterCantPlace() {
    console.log("instance")
    console.log(this.instance)
    this.instance.ifErrorX = this.instance.startPiece;

    console.log(this.ifErrorX)

    this.instance.pieceSelected.x = this.instance.ifErrorX.x;
    this.instance.pieceSelected.y = this.instance.ifErrorX.y;

    this.instance.tween = this.game.tweens.add({
      targets: this.instance,
      x: this.RATIO_GLOBAL_X * this.instance.ifErrorX.x,
      y: this.RATIO_GLOBAL_Y * this.instance.ifErrorX.y,
      duration: 1000,
      ease: "Cubic",
    });

    console.log("Goes in reset")

    this.setNewBlockInPlateauInstance();
    this.resetShadowPositionAfterCantPlace();
  }

  moveBlock() {
    this.instance.tween = this.game.tweens.add({
      targets: this.instance,
      x: this.instance.pieceSelected.x * this.RATIO_GLOBAL_X,
      y: this.instance.pieceSelected.y * this.RATIO_GLOBAL_Y,
      duration: 1000,
      ease: "Cubic",
    });

    this.setNewBlockInPlateauInstance();
  }

  positionateObject() {
    if (this.checkIfCanMove(this) === true) {
      this.moveBlock();
    } else {
      this.resetPositionAfterCantPlace();
    }
  }

  checkIfCanMove(_this) {
    let canPlace = true;

    console.log("CHECK IF CAN MOVE PLATEAU", _this.game.plateau.instance);

    _this.instance.schema.forEach((_row, idx) => {
      if (canPlace) {
        _row.forEach((_col, _idx) => {
            if (this.instance.pieceSelected) {
                const realY = _this.instance.pieceSelected.y + idx - 2;
                const realX = _this.instance.pieceSelected.x + _idx;
                console.log("COL", _col);
                if (_col !== " ") {
                  if (_this.game.plateau.instance[realY] && canPlace) {
                    if (_this.game.plateau.instance[realY]) {
                        if (_this.game.plateau.instance[realY][realX]) {
                            if (
                              _this.game.plateau.instance[realY][realX][0] !==
                                "x" &&
                              _this.game.plateau.instance[realY][realX][0] !==
                                _this.instance.letter
                            ) {
                              canPlace = false;
                            }
                        } else {
                            canPlace = false
                        }
                    } else {
                      canPlace = false;
                    }
                  } else {
                    canPlace = false;
                  }
                }
            } else {
                canPlace = false
            }
        });
      }
    });

    console.log("CanPlace", canPlace)
    return canPlace;
  }

  destroy() {
    this.instance.destroy();
    this.instance = null;
  }
}

export default ObjectToPlace
import { modifyArrayValue } from '../utils';
import Block from './block';

class Player extends Block {
    constructor(x, y) {
        super(x, y); // Appel du constructeur de la classe parente (Block)
    }

    setInstance() {
        // Utilisez 'player' au lieu de 'block'
        this.instance = this.game.physics.add.sprite(
            this.game.TILE_SCALE * this.game.scaleTileX * this.x,
            this.game.TILE_SCALE * this.game.scaleTileY * this.y,
            'player' // Remplacez 'block' par 'player'
        ).setInteractive({ draggable: true });
        this.instance.setOrigin(0, 0);
        this.instance.setScale(this.game.scaleTileX, this.game.scaleTileY);
    }

    checkIfCanMove() {
        if (["top", "bottom", "left", "right"].includes(this.moveDirection)) {
            const dx = { top: 0, bottom: 0, left: -1, right: 1 }[this.moveDirection];
            const dy = { top: -1, bottom: 1, left: 0, right: 0 }[this.moveDirection];

            const newX = this.x + dx;
            const newY = this.y + dy;

            if (this.game.plateau.instance[newY][newX] === " ") {
                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, newY, newX, "P"); // Remplacez 'C' par 'P'
                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ");
                this.x = newX;
                this.y = newY;

                this.tween = this.game.tweens.add({
                    targets: this.instance,
                    x: this.game.TILE_SCALE * this.game.scaleTileX * this.x,
                    y: this.game.TILE_SCALE * this.game.scaleTileY * this.y,
                    duration: 1000,
                    ease: 'Cubic'
                });
            } else if (this.game.plateau.instance[newY][newX] === "o") {
                this.game.isWin = true

                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, newY, newX, "C");
                this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ");

                this.x = newX;
                this.y = newY;

                this.tween = this.game.tweens.add({
                    targets: this.instance,
                    x: this.game.TILE_SCALE * this.game.scaleTileX * this.x,
                    y: this.game.TILE_SCALE * this.game.scaleTileY * this.y,
                    duration: 1000,
                    ease: 'Cubic',
                    onComplete: () => {
                        this.tween = this.game.tweens.add({
                            targets: this.instance,
                            delay: 4,
                            x: this.game.TILE_SCALE * this.game.scaleTileX * (this.x + 2),
                            y: this.game.TILE_SCALE * this.game.scaleTileY * (this.y),
                            duration: 1000,
                            ease: 'Cubic'
                        })
                    }
                })
            }
        }
    }
}

export default Player;
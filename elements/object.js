import MainGame from "../main"
import {
  modifyArrayValue,
  showPlateauInConsole,
  generateRandomString,
  coordToPixel,
  pixelPositionToCoord,
  getPixelPositionInArrayByCoords,
} from "../utils";

class ObjectToPlace {
  constructor(x, y, schema, assetName) {
    this.game = new MainGame();

    this.x = x;
    this.y = y;
    this.schema = schema
    this.assetName = assetName

    this.isDown = false;

    this.init();
  }

  init() {
    this.setInstance();
    this.setShadow();
    this.setInstancePosition(coordToPixel(this.x, this.game, true), coordToPixel(this.y, this.game, false))
    console.log(this.instance, coordToPixel(this.x, this.game, true), coordToPixel(this.y, this.game, false))
    this.setEventListeners();
  }

  setInstance() {
    this.scene = this.game.scene
    this.plateau = this.game.plateau
    this.container = this.game.container
    this.cases = this.container.cases
    this.global = this.game.global

    this.instance = this.game.physics.add.sprite(coordToPixel(this.x, this.game, true), coordToPixel(this.y, this.game, false), this.assetName)
      .setInteractive({ draggable: true, cursor: "pointer" });

    this.instance.defaultX = this.x;
    this.instance.defaultY = this.y;
    // this.instance = this.game.scene.add.rectangle(200, 200, 200, 200, 0xff0000).setInteractive({ draggable: true, cursor: "pointer" })
    this.instance.setOrigin(0, 0);
    this.instance.setDepth(2);
    this.instance.setScale(this.game.global.scaleRatioX, this.game.global.scaleRatioY);

    this.instance.schema = this.schema

    this.instance.position = {}
    this.instance.position.startPosition = {}
    this.instance.position.cursorDifference = {}
    this.instance.position.current = {}
    this.instance.position.selected = {}

    this.instance.letter = generateRandomString();

    while (this.game.letters.includes(this.instance.letter)) {
        this.instance.letter = generateRandomString();
    }

    this.instance.schema = this.instance.schema.map((_row, idx) => {
        return _row.map((_col, _idx) => {
            if (_col === "o") {
                return this.instance.letter
            } else {
                return " "
            }
        })
    })
  }

  setInstancePosition(x, y) {
    const position = getPixelPositionInArrayByCoords(this.game, x, y)
    console.log("???????????", x, y, position)
    this.instance.setPosition(position.x, position.y)
    this.instance.shadow.setPosition(position.x, position.y)
  }

  setShadow() {
    this.instance.shadow = this.scene.add.sprite(200, 200, this.assetName)
    this.instance.shadow.setOrigin(0, 0);
    this.instance.shadow.setDepth(3);
    this.instance.shadow.setScale(this.game.global.scaleRatioX, this.game.global.scaleRatioY);
    this.instance.shadow.alpha = 0.4;
  }

  browserSchemaAndPlace(coords) {
    this.schema.forEach((row, idx) => {
      row.forEach((col, _idx) => {
          if (col === "o") {
          this.addBlockInBoard({
            x: coords.x + _idx,
            y: coords.y + idx
          })
        }
      })
    })
  }

  eraseCurrentBlock() {
    this.schema.forEach((row, idx) => {
      row.forEach((col, _idx) => {
        if (col === "o") {
          this.plateau.board = modifyArrayValue(this.plateau.board, this.instance.position.startPosition.y + idx, this.instance.position.startPosition.x + _idx, "x")
        }
      })
    })
  }

  addBlockInBoard(coords) {
    this.plateau.board = modifyArrayValue(this.plateau.board, coords.y, coords.x, this.instance.letter)
  }

  checkIfCanAddBlock() {
    let canMove = true

    this.schema.forEach((row, idx) => {
      row.forEach((col, _idx) => {
        if (col === "o") {
          if (this.game.plateau.board[this.instance.position.selected.y + idx] && this.game.plateau.board[this.instance.position.selected.y + idx][this.instance.position.selected.x + _idx] === "x") {
          } else {
            canMove = false
          }
        }
      })
    })

    return canMove
  }

  onDragStart(pointer, dragX, dragY) {
    if (this.game.isWin === false) {
      this.instance.setDepth(3);

      this.instance.position.cursorDifference = {
        x: dragX,
        y: dragY,
      };

      // On stocke en coordonées la position de départ de l'object
      this.instance.position.startPosition.x = pixelPositionToCoord(pointer.position.x - dragX, this.game, true)
      this.instance.position.startPosition.y = pixelPositionToCoord(pointer.position.y - dragY, this.game, false)

      this.eraseCurrentBlock();
    }
  }

  onDrag(pointer) {
    if (this.game.isWin === false) {
      // On récupère la positon en PX de la pièce actuellement
      this.instance.position.current.x = pointer.x - this.instance.position.cursorDifference.x
      this.instance.position.current.y = pointer.y - this.instance.position.cursorDifference.y
      
      // On vérifie dans quelle position elle serait potentiellement lâchée
      this.instance.position.selected.x = pixelPositionToCoord(this.instance.position.current.x, this.game, true)
      this.instance.position.selected.y = pixelPositionToCoord(this.instance.position.current.y, this.game, false)
      
      // On set la shadow position et la position
      this.instance.shadow.setPosition(coordToPixel(this.instance.position.selected.x, this.game, true), coordToPixel(this.instance.position.selected.y, this.game, false))
      this.instance.setPosition(this.instance.position.current.x, this.instance.position.current.y);
    }
  }

  moveBlockTo(coords) {
    this.instance.tween = this.game.tweens.add({
      targets: this.instance,
      x: coordToPixel(coords.x, this.game, true),
      y: coordToPixel(coords.y, this.game, false),
      duration: 400,
      ease: "Cubic",
      onComplete: () => {
        console.log(this.instance.x)
      }
    })
  }

  onDragEnd() {
    if (this.game.isWin === false) {
      this.instance.setDepth(5); 

      if (this.checkIfCanAddBlock() === true) {
        this.moveBlockTo(this.instance.position.selected)
        this.browserSchemaAndPlace(this.instance.position.selected)
      } else {
        console.log(this.instance)
        console.log("D", this.instance.defaultX, this.instance.defaultY)
        this.instance.shadow.setPosition(coordToPixel(this.instance.defaultX, this.game, true), coordToPixel(this.instance.defaultY, this.game, false))
        this.moveBlockTo({
          x: this.instance.defaultX,
          y: this.instance.defaultY
        })
        this.browserSchemaAndPlace({
          x: this.instance.defaultX,
          y: this.instance.defaultY
        })
      }

      this.game.checkIfWin()
    }
  }

  setEventListeners() {
    this.instance.on("dragstart", (pointer, dragX, dragY) => this.onDragStart(pointer, dragX, dragY), this.game);
    this.instance.on("drag", (pointer) => this.onDrag(pointer), this.game);
    this.instance.on("dragend", () => this.onDragEnd(), this.game);
  }

  destroy() {
    this.instance.destroy();
    this.instance = null;
  }
}

export default ObjectToPlace
import MainGame from "../main"

class Bord {
    constructor(x, y) {
        this.game = new MainGame()

        this.x = x
        this.y = y


        this.init()
    }

    init() {
        this.setInstance()
    }
    
    setInstance() {
        const instanceX = this.game.container.cases.width * this.x + this.game.gutters.left
        const instanceY = this.game.container.cases.height * this.y + this.game.gutters.top
        
        this.instance = this.game.physics.add.sprite(instanceX, instanceY, 'obstacle')
        this.instance.setOrigin(0, 0)
        this.instance.setScale(this.game.global.scaleRatioX, this.game.global.scaleRatioY)
        this.instance.setDepth(2)
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Bord
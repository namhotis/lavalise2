    import MainGame from "../main"
    import { modifyArrayValue } from "../utils"
    import Block from "./block";

    class YBlock extends Block {
        constructor(x, y) {
            super(x, y)
        }

        setInstance() {
            // Utilisez 'player' au lieu de 'block'
            this.instance = this.game.physics.add.sprite(
            this.game.TILE_SCALE * this.game.scaleTileX * this.x,
            this.game.TILE_SCALE * this.game.scaleTileY * this.y,
            'y_block' // Remplacez 'block' par 'player'
            ).setInteractive({ draggable: true, cursor: 'pointer' });
            this.instance.setOrigin(0, 0);
            this.instance.setScale(this.game.scaleTileX, this.game.scaleTileY);
        }

        checkIfCanMove() {
            if (this.moveDirection === "top") {
                if (this.game.plateau.instance[this.y - 1][this.x] === " ") {
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y - 1, this.x, "z")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, "y")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x, " ")
                    this.y -= 1      
                    this.tween = this.game.tweens.add({ targets: this.instance, x: this.game.TILE_SCALE * this.game.scaleTileX * (this.x), y: this.game.TILE_SCALE * this.game.scaleTileY * (this.y), duration: 1000, ease: 'Cubic' })
                }
            } else if (this.moveDirection === "bottom") {
                if (this.game.plateau.instance[this.y + 2][this.x] === " ") {
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x, "z")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x, "y")
                    this.y += 1
                    this.tween = this.game.tweens.add({ targets: this.instance, x: this.game.TILE_SCALE * this.game.scaleTileX * (this.x), y: this.game.TILE_SCALE * this.game.scaleTileY * (this.y), duration: 1000, ease: 'Cubic' })
                }
            } else if (this.moveDirection === "left") {
                if (this.game.plateau.instance[this.y][this.x - 1] === " " && this.game.plateau.instance[this.y + 1][this.x - 1] === " ") {
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x - 1, "z")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x - 1, "y")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x, " ")
                    this.x -= 1      
                    this.tween = this.game.tweens.add({ targets: this.instance, x: this.game.TILE_SCALE * this.game.scaleTileX * (this.x), y: this.game.TILE_SCALE * this.game.scaleTileY * (this.y), duration: 1000, ease: 'Cubic' })
                }
            } else if (this.moveDirection === "right") {
                if (this.game.plateau.instance[this.y][this.x + 1] === " " && this.game.plateau.instance[this.y + 1][this.x + 1] === " ") {
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x + 1, "z")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x + 1, "y")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y, this.x, " ")
                    this.game.plateau.instance = modifyArrayValue(this.game.plateau.instance, this.y + 1, this.x, " ")
                    this.x += 1          
                    this.tween = this.game.tweens.add({ targets: this.instance, x: this.game.TILE_SCALE * this.game.scaleTileX * (this.x), y: this.game.TILE_SCALE * this.game.scaleTileY * (this.y), duration: 1000, ease: 'Cubic' })
                }
            }

            console.log(this.game.plateau.instance)
        }
    }

    export default YBlock
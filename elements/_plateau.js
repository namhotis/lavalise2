import MainGame from "../main"
import Bord from "./bord"

class Plateau {
    constructor() {
        this.game = new MainGame()
        this.tilesPlateau = []
        this.init()
    }

    init() {
        this.setInstance()
        this.defineMainSizes()
    }

        setInstance() {
            this.instance = [
              "xxxxxxxxx",
              "xxxxxxxxx",
              "xxxxxxxxx",
              "xxxxxxxxx",
              "xxxxxxxxx",
              "xxxxxxxxx",
            ];
            this.instance = this.instance.map((ligne) => {
                ligne = ligne.split("")
                ligne = ligne.map((letter) => {
                    return [letter] 
                })
                return ligne
            })
            console.log(this.instance)
    }
    
    defineMainSizes() {
        this.nombreCasesX = this.instance[0].length;
        this.nombreCasesY = this.instance.length;

        this.hauteurTotale = this.game.scale.baseSize.height
        this.largeurTotale = this.game.scale.baseSize.width;
        console.log("Hauteur totale", this.hauteurTotale)

        this.largeurPX = this.game.scale.baseSize.width
        this.hauteurPY = this.game.scale.baseSize.height - 200
        this.largeurCaseUnique = this.largeurPX / this.nombreCasesX
        this.hauteurCaseUnique = this.hauteurPY / this.nombreCasesY

        this.game.scaleTileX = this.largeurCaseUnique / this.game.TILE_SCALE
        this.game.scaleTileY = this.hauteurCaseUnique / this.game.TILE_SCALE

        this.generateTerrain()
    }

    generateTerrain() {
        this.instance.forEach((ligne, index) => {
            this.tilesPlateau.push([]);

            [...ligne].forEach((cell, _index) => {
                this.generateCell(cell, index, _index)
            })
        })
    }

    generateCell(cell, index, _index) {
        if (cell[0] === "x") {
            this.bord = new Bord(_index, index)
            cell.push(this.bord)
        } else {
            cell.push(null)
        }
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Plateau
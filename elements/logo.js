import MainGame from "../main"

class Logo {
    constructor() {
        this.game = new MainGame()

        this.init()
    }

    init() {
        this.setInstance()
        this.setEventListener()
    }

    setInstance() {
        this.instance = this.game.physics.add.sprite(400, 100, 'logo').setInteractive()
    }

    setEventListener() {
        this.instance.on('pointerdown', (pointer) => {
            this.moveToRight()
        });
    }

    moveToRight() {
        this.tween = this.game.tweens.add({ targets: this.instance, x: 100, duration: 1000, ease: 'Cubic' })
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Logo
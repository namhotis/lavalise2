import MainGame from "../main"
import Bord from "./bord"

const params = {
    height: 6,
    width: 8,
    gutterWidth: 150,
    assetSize: 200,
    gutters: {
        top: 174,
        right: 49,
        bottom: 154,
        left: 395
    }
}

class Plateau {
    constructor() {
        this.game = new MainGame()
        this.tilesPlateau = []
        this.init()
    }

    init() {
        this.setInstance()
        this.defineMainSizes()
        this.setInstanceSizes()
        this.generateTerrain()
    }

    setInstance() {
        this.instance = this.game.scene.add.rectangle(0, 0, 0, 0, 0xffffff);
        this.board = []

        for (let index = 0; index < params.width; index++) {
            let row = []

            for (let _index = 0; _index < params.height; _index++) {
                row.push(['x'])
            }

            this.board.push(row)
        }

        console.log(this.board)
    }

    defineMainSizes() {
        this.game.gutters = {}
        this.game.gutters = params.gutters

        this.game.global = {}
        this.game.global.width = this.game.scale.baseSize.width
        this.game.global.height = this.game.scale.baseSize.height

        this.game.container = {}
        this.game.container.width = this.game.global.width - (this.game.gutters.left + this.game.gutters.right)
        this.game.container.height = this.game.global.height - (this.game.gutters.top + this.game.gutters.bottom)
        console.log("a", this.game.container.height)
        this.game.container.xNbCases = this.board[0].length
        this.game.container.yNbCases = this.board.length

        // Place instance based on gutters
        this.instance.x = this.game.gutters.left
        this.instance.y = this.game.gutters.top

        // Définition de la taille de case
        this.game.container.cases = {}
        this.game.container.cases.width = this.game.container.width / this.game.container.xNbCases
        this.game.container.cases.height = this.game.container.height / this.game.container.yNbCases
        console.log(this.game.container.cases.height)

        // define scale ratio between assets and showed
        this.game.global.scaleRatioX = this.game.container.cases.width / params.assetSize
        this.game.global.scaleRatioY = this.game.container.cases.height / params.assetSize
    }

    setInstanceSizes() {
        this.instance.width = this.game.container.width
        this.instance.height = this.game.container.width
    }

    generateTerrain() {
        this.board.forEach((ligne, index) => {
            this.tilesPlateau.push([]);

            [...ligne].forEach((cell, _index) => {
                this.generateCell(cell, index, _index)
            })
        })

        
        this.board = this.board.map((ligne, index) => {
            return [...ligne].map((cell, _index) => {
                return "x"
            })
        })
    }

    generateCell(cell, index, _index) {
        if (cell[0] === "x") {
            this.bord = new Bord(_index, index)
            cell.push(this.bord)
        } else {
            cell.push(null)
        }
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Plateau
import MainGame from "../main"

class Emitter {
    constructor() {
        this.game = new MainGame()

        this.init()
    }

    init() {
        this.setInstance()
    }

    setInstance() {
        this.instance = this.game.add.particles(50, 50, "red", {
            speed: 100,
            scale: { start: 1, end: 0 },
            blendMode: "ADD",
          });
    }

    destroy() {
        this.instance.destroy()
        this.instance = null
    }
}

export default Emitter
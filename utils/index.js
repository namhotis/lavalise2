

const modifyArrayValue = (arr, rowIndex, colIndex, newValue) => {
    // Vérifier si les indices sont valides
    if (
        rowIndex < 0 ||
        rowIndex >= arr.length ||
        colIndex < 0 ||
        colIndex >= arr[0].length
    ) {
        console.log({
          message: 'Indices non valides', 
          rowIndex: rowIndex, 
          colrIndex: colIndex,
          value: newValue,
          tests: [rowIndex < 0,
            rowIndex >= arr.length,
            colIndex < 0,
            colIndex >= arr[0].length]
        });
        return arr;
    }

    const newArray = [...arr];
    const newRow = [...newArray[rowIndex]];
    newRow[colIndex] = newValue;
    newArray[rowIndex] = newRow;

    return newArray;
}

const showPlateauInConsole = (plateau) => {
    let _plateau = plateau

    _plateau = _plateau.map((row) => {
        return row
          .map((col) => {
            return col[0];
          })
          .join();
    })

    return _plateau
}

const generateRandomString = () => {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWYZ";
  let result = "";
  for (let i = 0; i < 1; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    result += characters[randomIndex];
  }
  return result;
}

const pixelPositionToCoord = (value, game, isX) => {
  return Math.round((value -  (isX ? game.gutters.left : game.gutters.top)) / (isX ? game.container.cases.width : game.container.cases.height))
}

const coordToPixel = (value, game, isX) => {
  let baseValue = value * game.container.cases.width
  let paddingvalue = isX ? game.gutters.left : game.gutters.top
  return baseValue + paddingvalue
}

const getPixelPositionInArrayByCoords = (game, x, y) => {
  const position = {
    x: null,
    y: null
  }

  // Get X and Y case by PX
  const coordsPosition = {
    x: pixelPositionToCoord(x, game, true),
    y: pixelPositionToCoord(y, game, false)
  }

  position.x = coordToPixel(coordsPosition.x, game, true)
  position.y = coordToPixel(coordsPosition.y, game, false)

  return position
}

export { modifyArrayValue, showPlateauInConsole, generateRandomString, getPixelPositionInArrayByCoords, coordToPixel, pixelPositionToCoord };
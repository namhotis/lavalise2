import Phaser from 'phaser';
import Plateau from './elements/plateau';
import ObjectToPlace from './elements/object';

let game;
let instance = null
let config = null;
let isDone = false;

class MainGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");

        // Singleton
        if (instance) {
            return instance
        }
        instance = this

        this.TILE_SCALE = 200
    }

    preload() {
        this.load.image('background', '/assets/bg/BACKGROUND_extended_desktop.jpg')
        this.load.image("jumelles_block", "./assets/obstacle/jumelles_block.png");
        this.load.image("nourriture_block", "./assets/obstacle/nourriture_block.png");
        this.load.image("bottes_block", "./assets/obstacle/bottes_block.png");
        this.load.image("thermos_block", "./assets/obstacle/thermos_block.png");
        this.load.image("vetement_block", "./assets/obstacle/vetement_block.png");
        this.load.image("pioche_block", "./assets/obstacle/pioche_block.png");
        this.load.image("corde_block", "./assets/obstacle/corde_block.png");
        this.load.image("lampe_block", "./assets/obstacle/lampe_block.png");

        this.scene = game.scene.scenes[0]
    }

    create() {
        this.isWin = false
        this.letters = []

        this.plateau = new Plateau()

        this.background = this.physics.add.sprite(0, 0, 'background')
        this.background.setOrigin(0, 0)
        this.background.setScale(this.global.width / this.background.width, this.global.height / this.background.height)
        this.background.setDepth(0)

        this.jumelles = new ObjectToPlace(-6, -2, [
            ["o", "o", "o"],
            ["o", "o", " "],
            ["o", "o", "o"]
        ], "jumelles_block")

        this.nourriture = new ObjectToPlace(-2, 6, [
            [" ", "o"],
            ["o", "o"],
            [" ", "o"]
        ], "nourriture_block")

        this.bottes = new ObjectToPlace(-2, -2, [
            ["o", "o"],
            ["o", " "],
            ["o", " "]
        ], "bottes_block")

        this.thermos = new ObjectToPlace(-4, 1, [
            ["o"],
            ["o"],
            ["o"],
            ["o"]
        ], "thermos_block")

        this.vetement = new ObjectToPlace(-6, 5, [
            ["o", "o", "o"],
            ["o", "o", "o"],
            ["o", "o", "o"],
        ], "vetement_block")

        this.pioche = new ObjectToPlace(-6, 7, [
            [" ", " ", " ", "o"],
            ["o", "o", "o", "o"],
            [" ", " ", " ", "o"],
        ], "pioche_block")

        this.corde = new ObjectToPlace(-3, 2, [
            ["o", "o"],
            ["o", "o"],
            ["o", "o"],
            ["o", "o"],
            ["o", "o"],
        ], "corde_block")

        this.lampe = new ObjectToPlace(1, -2, [
            ["o", "o", "o"]
        ], "lampe_block")
    }  
    
    checkIfWin() {
        let xLength = 0

        this.plateau.board.forEach(element => {
            element.forEach(el => {
                console.log(el)
                if (el === "x") {
                    xLength += 1
                }
            })
        });

        if (xLength === 0) {
            this.win()
        }
    }

    win() {
        this.isWin = true

        setTimeout(() => {
            this.winBg = this.add.rectangle(0, 0, this.global.width, this.global.height, 0xffffff);
            this.winBg.alpha = 0.7
            this.winBg.setOrigin(0, 0)
            this.winBg.setDepth(10)

            this.winText = this.add.text(this.global.width/2, this.global.height/2, 'Félicitations');
            this.winText.setStyle({
                fontSize: '64px',
                // align: 'center',
                color: 'black',
            })
            this.winText.setOrigin(0.5, 0.5)
            this.winText.setDepth(11)
        }, 300)

    }
}

window.addEventListener('DOMContentLoaded', () => {
    const launchGame = () => {
        config = {
          type: Phaser.CANVAS,
          parent: "testcanvas",
          width: 800,
          height: 800,
          scale: {
            mode: Phaser.Scale.FIT,
            parent: "testcanvas",
            autoCenter: Phaser.Scale.CENTER_BOTH,
            maxWidth: 800,
            maxHeight: 800,
          },
          physics: {
            default: "arcade",
            arcade: {
              gravity: {
                y: 0,
              },
              debug: false,
            },
          },
          scene: MainGame,
        };

        game = new Phaser.Game(config);

        isDone = true
    }

    if (isDone === false) {
        launchGame()
    }
})

export default MainGame
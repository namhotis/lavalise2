import Phaser from 'phaser';
import Plateau from './elements/plateau';
import ObjectToPlace from './elements/object';

let game;
let instance = null
let config = null;
let isDone = false;

class MainGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");

        // Singleton
        if (instance) {
            return instance
        }
        instance = this

        this.TILE_SCALE = 200
    }

    preload() {
        this.load.image('sky', '/assets/space3_1.jpg');
        this.load.image('logo', '/assets/phaser3-logo.png');
        this.load.image('red', '/assets/obstacle/soprano.png');
        this.load.image('player', '/assets/obstacle/base-title_200x200_joueur.jpg')
        this.load.image('obstacle', '/assets/obstacle/base-tile_200x200.jpg')
        this.load.image('block', '/assets/obstacle/base-title_200x200_block.jpg')
        this.load.image('big_block', '/assets/obstacle/base-title_400x400_big_block.jpg')
        this.load.image('x_block', '/assets/obstacle/x-tile_400x200.jpg')
        this.load.image('y_block', '/assets/obstacle/y-tile_200x400.jpg')
        this.load.image("L_block", "/assets/obstacle/l-tile_400x200.png");
        this.load.image(
            "Poelle_block",
            "/assets/obstacle/poelle-tile_800x600.png"
        );

        this.scene = game.scene.scenes[0]
    }

    create() {
        this.letters = []

        this.background = this.add.image(0, 0, 'sky');
        this.background.setOrigin(0, 0)
        this.plateau = new Plateau()

        this.objet = new ObjectToPlace(2, -2, [
            ["o", "o"],
            ["o", "o"]
        ], "big_block");

        this.objet2 = new ObjectToPlace(4, -2, [ 
            ["o", "o"],
            ["o", "o"],
        ], "big_block");

        this.objet3 = new ObjectToPlace(0, -2, [
            ["o", " "],
            ["o", "o"],
        ], "L_block");

        this.objet4 = new ObjectToPlace(
            5,
            -2,
            [
                ["o", "o", " ", " "],
                ["o", "o", "o", "o"],
                ["o", "o", " ", " "],
            ],
            "Poelle_block"
        );

        this.objet5 = new ObjectToPlace(
            1,
            -2,
            [
                ["o", "o", " ", " "],
                ["o", "o", "o", "o"],
                ["o", "o", " ", " "],
            ],
            "Poelle_block"
        );

        this.objet6 = new ObjectToPlace(
            3,
            -2,
            [
                ["o", "o"],
                ["o", "o"],
            ],
            "big_block"
        );
    }
}

window.addEventListener('DOMContentLoaded', () => {
    const launchGame = () => {
        config = {
          type: Phaser.CANVAS,
          parent: "testcanvas",
          width: 800,
          height: 800,
          scale: {
            mode: Phaser.Scale.FIT,
            parent: "testcanvas",
            autoCenter: Phaser.Scale.CENTER_BOTH,
            maxWidth: 800,
            maxHeight: 800,
          },
          physics: {
            default: "arcade",
            arcade: {
              gravity: {
                y: 0,
              },
              debug: false,
            },
          },
          scene: MainGame,
        };

        game = new Phaser.Game(config);

        isDone = true
    }

    if (isDone === false) {
        launchGame()
    }
})

export default MainGame